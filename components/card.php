<div class="column is-one-third">
    <div class="card">
        <div class="card-header">
            <p class="card-header-title">
                <?= "#" . $number . ". " . $card["title"] ?>
            </p>
            <button class="card-header-icon modal-btn-open" aria-label="more options" data-id="<?=$card['id'];?>">
                <?php
                $score_dict = array("is-dark", "is-dark", "is-dark", "is-danger", "is-warning", "is-success");
                $score = $card['score'];
                ?>
                <span class="tag <?=$score_dict[$score]?>"><?=$score > 0 ? $score : "Ожидает проверку"?></span>
            </button>
        </div>
        <div class="card-content">
            <div class="content">
                <?= $card['description'] ?>
                <br>
                <?php
                    $tags = explode(',', $card["tags"]);
                    foreach ($tags as $tag) echo '<a href="#">#' . $tag . '</a>';
                ?>
                <br>
                <?php
                    $date = new DateTime($card['date']);
                ?>
                <time datetime="<?= $card['date'] ?>"><?= $date->format('j F Y'); ?></time>
            </div>
        </div>
        <div class="card-footer">
            <a href="<?= $card['gitlab_link'] ?>" target="_blank" class="card-footer-item">
                <span class="icon">
                    <i class="fab fa-gitlab"></i>
                </span>
                <span>GitLab</span>
            </a>
            <a href="<?= $card['site_link'] ?>" target="_blank" class="card-footer-item">Сайт</a>
            <a href="<?=$HOSTNAME?>/pages/task.php?id=<?= $card['id'] ?>" class="card-footer-item">Подробнее</a>
        </div>
    </div>
</div>