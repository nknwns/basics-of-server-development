<h5><?= $lecture["name"] ?></h5>
<div class="block"><?= $lecture["content"] ?></div>
<div class="block">
<?php
    $count = 1;
    foreach (explode(',', $lecture['files']) as $file_link) {
        ?>
            <a class="button is-warning" href="<?=$file_link?>" target="_blank">Файл #<?=$count?></a>
        <?php
        $count++;
    }
?>
</div>