<div class="column is-one-third">
    <div class="card load-card">
        <div class="card-header">
            <p class="card-header-title">
                + Новое задание
            </p>
            <button class="card-header-icon" aria-label="more options">
                <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                </span>
            </button>
        </div>
        <div class="card-content">
            <a href="<?= $HOSTNAME ?>/pages/add-card-interface.php" class="button is-medium fa-beat" style="--fa-animation-duration: 2s;">
                <span class="icon">
                    <i class="fa-solid fa-plus fa-2x"></i>
                </span>
            </a>
        </div>
        <div class="card-footer">
            <a class="card-footer-item">
                <span class="icon">
                    <i class="fab fa-gitlab"></i>
                </span>
                <span>GitLab</span>
            </a>
            <a class="card-footer-item">Сайт</a>
            <a class="card-footer-item">Подробнее</a>
        </div>
    </div>
</div>