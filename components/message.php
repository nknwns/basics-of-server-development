<?php
$titles = array(
    "add" => "Задание успешно добавлено",
    "change" => "Информация о задании успешно обноавлена"
);

if ($_GET['status'] == "success") {
    ?>
    <article class="message is-success">
        <div class="message-header">
            <p><?=$titles[$_GET['type']]?></p>
            <button class="delete delete-message" aria-label="delete"></button>
        </div>
    </article>
    <?php
} elseif ($_GET['status'] == "danger") {
    ?>
    <article class="message is-danger">
        <div class="message-header">
            <p>Произошла ошибка. Проверьте корректность ввода пароля и данных</p>
            <button class="delete delete-message" aria-label="delete"></button>
        </div>
    </article>
    <?php
}
?>