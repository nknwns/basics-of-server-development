<footer class="footer">
    <div class="content has-text-centered">
        <p>
            Created by Andrej Shaev, student <a href="https://mospolytech.ru/">Moscow Polytech</a>.
        </p>
    </div>
</footer>
<?php if ($is_task_page) { ?> <script src="js/main.js"></script> <?php } ?>
<script src="<?=$HOSTNAME?>/js/main.js"></script>
</body>
</html>