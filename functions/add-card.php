<?php
if (isset($_POST['add'])) {

  if (md5($_POST['password']) != "662d7981ecf0b8df1c5edd425d63bec9") {
    header("Location: ../pages/tasks.php?status=danger");
    exit;
  }

  include("../database.php");

  $lecture_links = $_POST['lectures'];
  $is_lectures = strlen($lecture_links) != 0;

  if ($is_lectures) {
    $request = "INSERT INTO lectures (name, content, files) VALUES ('";
    $request .= $_POST['lecture_name'] . "', '";
    $request .= $_POST['lecture_description'] . "', '";
    $request .= $lecture_links . "')";

    mysqli_query($database, $request);
  }

  $lecture_id = $database->insert_id;

  $request = "INSERT INTO tasks (title, description, date, tags, content, gitlab_link, site_link, lecture_ids) VALUES ('";
  $request .= $_POST['title'] . "', '";
  $request .= $_POST['description'] . "', '";
  $request .= $_POST['date'] . "', '";
  $request .= $_POST['tags'] . "', '";
  $request .= $_POST['content'] . "', '";
  $request .= $_POST['gitlab'] . "', '";
  $request .= $_POST['site'] . "', '";

  if ($is_lectures) {
    $request .= $lecture_id . "')";
  } else {
    $request .= "')";
  }

  mysqli_query($database, $request);

  header("Location: ../pages/tasks.php?status=success&type=add");
  exit;
} else {
  header("Location: ../pages/tasks.php");
  exit;
}
?>