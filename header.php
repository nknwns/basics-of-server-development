<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Andrej Shaev. <?=$page_title?></title>
    <link rel="stylesheet" type="text/css" href="<?=$HOSTNAME?>/css/reseter.css">
    <link rel="stylesheet" type="text/css" href="<?=$HOSTNAME?>/css/bulma.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$HOSTNAME?>/css/style.css">
    <?php if ($is_task_page) { ?> <link rel="stylesheet" type="text/css" href="css/style.css"> <?php } ?>
    <script src="https://kit.fontawesome.com/587a8b252f.js" crossorigin="anonymous"></script>
</head>
<body>
    <header class="header">
        <nav class="navbar is-info" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="https://mospolytech.ru/" target="_blank">
                    <img src="<?=$HOSTNAME?>/img/logo.png">
                </a>

                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="<?=$HOSTNAME?>">
                        Главная
                    </a>

                    <a class="navbar-item" href="<?=$HOSTNAME?>/pages/tasks.php">
                        Задания
                    </a>

                    <?php
                        if ($is_task_page) {
                            ?>
                                <div class="navbar-item">
                                    Текущее задание: <?=$task_name?>
                                </div>
                            <?php
                        }
                    ?>
                </div>
            </div>
        </nav>
    </header>