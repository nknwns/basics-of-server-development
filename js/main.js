const getParentElementByName = (element, name) => {
    if (element.classList.contains(name)) return element;
    return getParentElementByName(element.parentElement, name);
}

if (document.querySelector('.delete-message')) {
    document.querySelector('.delete-message').addEventListener("click", evt => {
        const message = getParentElementByName(evt.target, "message");
        message.remove();
    });
}

document.querySelectorAll(".modal-btn-open").forEach(el => {
    el.addEventListener("click", evt => {
        const button = getParentElementByName(evt.target, "modal-btn-open");
        const id = button.dataset.id;

        document.querySelector('.modal').classList.add('is-active');

        document.querySelector('.score-id').value = id;
    });
});

document.querySelectorAll(".modal-btn-close").forEach(el => {
    el.addEventListener("click", evt => {
        document.querySelector(".modal").classList.remove('is-active');
    });
});