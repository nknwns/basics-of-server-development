<div class="modal modal-score">
    <div class="modal-background modal-btn-close"></div>
    <div class="modal-content">
        <div class="box">
            <h2 class="title title-center">Изменение оценки</h2>
            <form action="<?=$HOSTNAME?>/functions/change-score.php" method="POST">
                <div class="field hidden">
                    <div class="control">
                        <input class="score-id" type="text" name="card_id" value="-1">
                    </div>
                </div>
                <div class="field">
                    <div class="control has-icons-left">
                        <div class="select">
                            <select name="score">
                                <option value="0" selected>Оценка</option>
                                <option value="2">Ты дурак - 2</option>
                                <option value="3">Пойдет - 3</option>
                                <option value="4">Хорошо - 4</option>
                                <option value="5">Отлично - 5</option>
                            </select>
                        </div>
                        <div class="icon is-small is-left">
                            <i class="fa-solid fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="field is-expanded">
                    <div class="field has-addons">
                        <p class="control is-expanded has-icons-left">
                            <input required class="input is-danger" type="password" placeholder="Пароль" name="password">
                            <span class="icon is-small is-left">
                                              <i class="fas fa-lock"></i>
                                            </span>
                        </p>
                    </div>
                    <p class="help is-danger">
                        Это поле обязательно для заполнения
                    </p>
                </div>
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-primary" name="change">
                            Сохранить
                        </button>
                    </div>
                    <div class="control">
                        <a href="#" class="button modal-btn-close is-danger" name="close">
                            Отменить
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <button class="modal-close modal-btn-close is-large" aria-label="close"></button>
</div>