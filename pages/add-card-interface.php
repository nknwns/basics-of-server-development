<?php
    include($_SERVER['DOCUMENT_ROOT'] . '/config.php');

    $page_title = "Создание нового задания";
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>
  <main>
    <section class="section">
      <div class="container">
        <form action="<?= $HOSTNAME ?>/functions/add-card.php" method="POST">
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Заголовок</label>
            </div>
            <div class="field-body">
              <div class="field">
                <p class="control">
                  <input required class="input is-danger" type="text" placeholder="Название" name="title">
                </p>
                <p class="help is-danger">
                  Это поле обязательно для заполнения
                </p>
              </div>
              <div class="field">
                <p class="control">
                  <input required class="input is-danger" type="text" placeholder="Краткое описание" name="description">
                </p>
                <p class="help is-danger">
                  Это поле обязательно для заполнения
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Теги задания</label>
            </div>
            <div class="field-body">
              <div class="field is-expanded">
                <div class="field has-addons">
                  <p class="control">
                    <a class="button is-static">
                      #
                    </a>
                  </p>
                  <p class="control is-expanded">
                    <input class="input" type="text" placeholder="тег, еще один тег, опять тег" name="tags">
                  </p>
                </div>
                <p class="help">Перечисляйте теги через запятую с маленькой буквы</p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Дата сдачи</label>
            </div>
            <div class="field-body">
              <div class="field is-narrow">
                <p class="control">
                  <input class="input is-danger" type="date" name="date" required>
                </p>
                <p class="help is-danger">
                  Это поле обязательно для заполнения
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Ссылки на ресурсы</label>
            </div>
            <div class="field-body">
              <div class="field">
                <p class="control is-expanded has-icons-left">
                  <input required class="input is-danger is-link" type="text" placeholder="GitLab" name="gitlab">
                  <span class="icon is-small is-left">
                    <i class="fab fa-gitlab"></i>
                  </span>
                </p>
                <p class="help is-danger">
                  Это поле обязательно для заполнения
                </p>
              </div>
              <div class="field">
                <p class="control is-expanded has-icons-left">
                  <input required class="input is-danger is-link" type="text" placeholder="Site" name="site">
                  <span class="icon is-small is-left">
                    <i class="fas fa-link"></i>
                  </span>
                </p>
                <p class="help is-danger">
                  Это поле обязательно для заполнения
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Лекция</label>
            </div>
            <div class="field-body">
              <div class="field is-narrow">
                <p class="control">
                  <input class="input" type="text" placeholder="Название" name="lecture_name">
                </p>
              </div>
              <div class="field">
                <p class="control">
                  <input class="input" type="text" placeholder="Описание лекции" name="lecture_description">
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Файлы лекции</label>
            </div>
            <div class="field-body">
              <div class="field is-expanded">
                <div class="field has-addons">
                  <p class="control is-expanded has-icons-left">
                    <input class="input is-link" type="text" placeholder="Site" name="lectures">
                    <span class="icon is-small is-left">
                      <i class="fa-solid fa-file-word"></i>
                    </span>
                  </p>
                </div>
                <p class="help">Перечисляйте ссылки на файлы через запятую</p>
              </div>
            </div>
          </div>
        
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Содержание</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control">
                  <textarea class="textarea" placeholder="Описание блока с изучаемой темой" name="content"></textarea>
                </div>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label">Пароль администратора</label>
            </div>
            <div class="field-body">
              <div class="field is-expanded">
                <div class="field has-addons">
                  <p class="control is-expanded has-icons-left">
                    <input class="input is-link" type="password" placeholder="Пароль" name="password">
                    <span class="icon is-small is-left">
                      <i class="fas fa-lock"></i>
                    </span>
                  </p>
                </div>
                <p class="help is-danger">
                    Это поле обязательно для заполнения
                </p>
              </div>
            </div>
          </div>

          <div class="field is-horizontal">
            <div class="field-label"></div>
            <div class="field-body">
              <div class="field">
                <div class="control">
                  <button class="button is-primary" name="add">
                    Сохранить
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </main>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>