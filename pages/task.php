<?php
    $task_id = $_GET['id'];

    $page_title = "Задание #" . $task_id; 
    $is_task_page = true;
    
    include ($_SERVER['DOCUMENT_ROOT'] . "/database.php");
    include ($_SERVER['DOCUMENT_ROOT'] . '/config.php');
    $task_response = mysqli_query($database, "SELECT * FROM tasks WHERE id=${task_id}");
    $task = mysqli_fetch_assoc($task_response);

    $task_name = $task["title"];

    $lectures_response = mysqli_query($database, "SELECT * FROM lectures WHERE id IN (" . $task["lecture_ids"] . ")");
    

?>

<?php include ($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

    <main>
        <div class="modal is-active">
            <div class="modal-background"></div>
            <div class="modal-card">
                <div class="modal-card-head">
                    <p class="modal-card-title"><?= $task["title"] ?></p>
                    <a href="<?= $HOSTNAME ?>/pages/tasks.php"><button class="delete" aria-label="close"></button></a>
                </div>
                <section class="modal-card-body">
                <div class="content">
                    <h1>Описание раздела</h1>
                    <?=$task["content"]?>
                    <hr>
                    <h2>Лекции</h2>
                    <?php
                        while ($lecture = mysqli_fetch_assoc($lectures_response)) {
                            include ($_SERVER['DOCUMENT_ROOT'] . "/components/lecture.php");
                        }
                    ?>
                </div>
                </section>
                <div class="modal-card-foot">
                    <a href="<?= $card['gitlab_link'] ?>" target="_blank" class="button is-info">
                        <span class="icon">
                            <i class="fab fa-gitlab"></i>
                        </span>
                        <span>GitLab</span>
                    </a>
                    <a href="<?= $task['site_link'] ?>" target="_blank" class="button is-info">Сайт</a>
                    <a href="<?= $HOSTNAME ?>/pages/tasks.php" class="button is-danger">Закрыть</a>
                </div>
            </div>
        </div>
    </main>

<?php include ($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>