<?php
    include_once ($_SERVER['DOCUMENT_ROOT'] . '/config.php');
    include_once ($_SERVER['DOCUMENT_ROOT'] . '/database.php');

    $page_title = "Задания";
    $tasks_response = mysqli_query($database, "SELECT * FROM tasks");

    include ($_SERVER['DOCUMENT_ROOT'] . '/header.php');
    include ($_SERVER['DOCUMENT_ROOT'] . '/components/message.php');

?>

    <main>
        <section class="section tasks">
            <div class="container">
                <div class="columns is-multiline">
                    <?php
                        $number = 1;

                        while ($card = mysqli_fetch_assoc($tasks_response)) {
                            include ($_SERVER['DOCUMENT_ROOT'] . '/components/card.php');

                            $number++;
                        }

                        include ($_SERVER['DOCUMENT_ROOT'] . '/components/load-card.php');
                    ?>
                </div>
                
            </div>
        </section>

        <?php include ($_SERVER['DOCUMENT_ROOT'] . '/modals/change-score.php'); ?>
    </main>


<?php include ($_SERVER['DOCUMENT_ROOT'] . '/footer.php') ?>
