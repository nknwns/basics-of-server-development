<?php
    require ('trig.php');

    function zeroDivisionError() {
        echo '{"status": false, "msg": "Ошибка. Деление на ноль >~<"}';
        exit;
    }

    function sendError($msg) {
        echo '{"status": false, "msg": "Ошибка. ' . $msg . '"}';
        exit;
    }

    function send($msg) {
        echo '{"status": true, "msg": "' . $msg . '"}';
    }

    function calculate($data) {
        while (isTrig($data)) trig(getTrig($data), $data);

        if (strpos($data, "(") !== false) return make($data);
        else return calc($data);
    }

    function make($data) {
        $endPos = strpos($data, ")");
        $startPos = $endPos;
        while ($data[$startPos] != "(") $startPos--;
        $diff = $endPos - $startPos;
        $currentData = substr($data, $startPos + 1, $diff - 1);
        $result = substr($data, 0, $startPos) . calc($currentData) . substr($data, $endPos + 1);
        if (strpos($result, "(") !== false) return make($result);
        else return calc($result);
    }

    function calc($data) {
        $data .= "+";

        $result = 0;
        $current = 0;
        $prevOperation = "+";
        $startDiffOperation = "";
        $temp = 0;

        for ($i = 0; $i < strlen($data); $i++) {
            if ($data[$i] == "-" && (in_array($data[$i - 1], array("*", "/", "+")))) {
                $current = "-";
            }
            elseif (isOperation($data[$i])) {
                $current = (float)$current;
                if (isSimpleOperation($data[$i])) {
                    switch ($prevOperation) {
                        case "+": {
                            $result += $current;
                            break;
                        }
                        case "-": {
                            $result -= $current;
                            break;
                        }
                        case "*": {
                            $temp *= $current;
                            if ($startDiffOperation == "+") $result += $temp;
                            else $result -= $temp;
                            $temp = 0;
                            break;
                        }
                        case "/": {
                            if (!$current) zeroDivisionError();
                            $temp /= $current;
                            if ($startDiffOperation == "+") $result += $temp;
                            else $result -= $temp;
                            $temp = 0;
                            break;
                        }
                    }
                } else {
                    if (isSimpleOperation($prevOperation)) {
                        $temp = $current;
                        $startDiffOperation = $prevOperation;
                    } else {
                        if ($prevOperation == "*") $temp *= $current;
                        else {
                            if (!$current) zeroDivisionError();
                            $temp /= $current;
                        }
                    }
                }

                $prevOperation = $data[$i];
                $current = "";
            } else {
                $current .= $data[$i];
            }
        }

        return $result;

    }

    function check($data) {
        // zero
        if (strpos($data, "/0") !== false) zeroDivisionError();

        // operation
        $operations = array("+", "-", "*", "/");
        $isEmptyOperation = true;
        foreach ($operations as $operation) {
            if (strpos($data, $operation) !== false) $isEmptyOperation = false;
        }
        if ($isEmptyOperation && !isTrig($data)) sendError("А делать то что надо? =/");

        // bad operation-position
        if ((isOperation($data[0]) && $data[0] != "-") || isOperation($data[strlen($data) - 1])) sendError("Может стоит дописать пример до конца?");

        // one more operation
        for ($i = 0; $i < strlen($data) - 1; $i++) {
            if (isOperation($data[$i]) && isOperation($data[$i + 1])) {
                if (!(in_array($data[$i], array("+", "*", "/")) && $data[$i + 1] == "-")) {
                    sendError("Кто-то украл число между операциями =0");
                }
            }
        }

        // count brackets and position brackets
        $bracketsPairs = 0;

        $stack = array();
        for ($i = 0; $i < strlen($data); $i++) {
            if ($data[$i] == "(") $bracketsPairs++;
            elseif ($data[$i] == ")") {
                $bracketsPairs--;
                if ($bracketsPairs < 0) sendError("Лее, а открывать скобку кто будет?");
            }
        }

        if ($bracketsPairs) sendError("Кажется, скобочек не хватает");



    }

    function isOperation($symbol) {
        return in_array($symbol, array("+", "-", "/", "*"));
    }

    function isSimpleOperation($symbol) {
        return in_array($symbol, array("+", "-"));
    }

    function isSpecific($symbol) {
        return in_array($symbol, array("(", ")"));
    }

    if ($_POST['submit']) {
        $request = $_POST['data'];

        check($request);

        $result = calculate($request);

        send($result);
        exit;
    }

?>