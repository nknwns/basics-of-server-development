<?php

include($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$page_title = "Задание #5. Calculator";
$is_task_page = true;
$task_name = "Calculator";

?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

    <main>
        <section class="section">
            <div class="container">
                <div class="columns">
                    <div class="column is-3 is-offset-3">
                        <form class="form-calculator" action="<?=$HOSTNAME?>/tasks/calculator/calc.php" method="post">
                            <div class="box box--calculator">
                                <label for="calculator-preview" class="label">Калькулятор</label>
                                <div class="field field-tab">
                                    <div class="calculator calculator--main">
                                        <div class="control">
                                            <input type="text" name="main" class="input calculator-preview" disabled>
                                        </div>
                                    </div>
                                </div>
                                <button class="button is-primary button--result">Решить</button>
                            </div>
                        </form>
                    </div>
                    <div class="column is-3">
                        <div class="box history">
                            <div class="menu">
                                <p class="menu-label">История запросов</p>
                                <ul class="menu-list">
                                </ul>
                            </div>
                            <button class="button is-danger is-small button--clear">Очистить</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <div class="notification is-danger">
        <button class="delete"></button>
        <p></p>
    </div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>