const FORM = document.querySelector('.form-calculator');

class HistoryObject {
    constructor(title, result) {
        this.title = title;
        this.result = result;

        this.preview = {
            title: title,
            result: result
        };

        if (this.title.length > 20) this.preview.title = this.title.substr(0, 20) + "...";
        if (this.result.length > 7) this.preview.result = this.result.substr(0, 7) + "...";
    }

    getHTML() {
        const elementParent = createElement("li", "history-object");
        const elementWrapper = createElement('a', "history-description");
        elementWrapper.dataset.title = this.title;
        const elementPreview = createElement("span", null, this.preview.title);
        const elementResult = createElement("span", "history-result", "=" + this.preview.result);
        const elementButton = createElement("button", "history-delete delete");


        elementWrapper.appendChild(elementPreview);
        elementWrapper.appendChild(elementResult);
        elementParent.appendChild(elementWrapper);
        elementParent.appendChild(elementButton);

        return elementParent;
    }
}

class Calculator {
    constructor(className) {
        this.object = document.querySelector(`.calculator--${className}`);
        this.mainInput = this.object.querySelector('.input.calculator-preview')
    }

    handlerOperation(evt) {
        evt.preventDefault();

        this.mainInput.value += evt.target.textContent;

        if (evt.target.classList.contains("button--trig")) this.mainInput.value += "(";

        if (this.mainInput.value.includes("/0")) FORM.querySelector('.button--result').classList.add("rat");
        else FORM.querySelector('.button--result').classList.remove("rat")
    }

    addHandlersByOperations(objects) {
        objects.forEach(el => {
            if (el.classList.contains("button-handle")) el.addEventListener("click", this.handlerOperation.bind(this));
        });
    }

    getGUI() {
        const GUI = createElement("div", "GUI");

        let buttons = document.createDocumentFragment();
        for (let i = 0; i < 10; i++) buttons.appendChild(createElement("button", "button button--operation button-handle", i.toString()));

        const clearOneElementButton = createElement("button", "button button--operation", "<-");
        clearOneElementButton.addEventListener("click", evt => {
            evt.preventDefault();
            if (["cos(", "sin(", "tan("].includes(this.mainInput.value.substr(-4))) this.mainInput.value = this.mainInput.value.slice(0, -4);
            else this.mainInput.value = this.mainInput.value.slice(0, -1);
        });
        buttons.appendChild(clearOneElementButton);

        const clearButton = createElement("button", "button button--operation", "C");
        clearButton.addEventListener("click", evt => {
            evt.preventDefault();
            this.mainInput.value = "";
        });
        buttons.appendChild(clearButton);

        const trigOperations = ["cos", "sin", "tan"];
        trigOperations.forEach(el => buttons.appendChild(createElement("button", "button button--operation button--trig button-handle", el)));

        const operations = ["+", "-", "*", "/", "(", ")"];
        operations.forEach(el => buttons.appendChild(createElement("button", "button button--operation button-handle", el)));

        this.addHandlersByOperations(buttons.childNodes);

        GUI.appendChild(buttons);
        return GUI;
    }

    render() {
        this.object.appendChild(this.getGUI());
    }


}

const createElement = (tag, className = null, content = null) => {
    const element = document.createElement(tag);
    if (className) element.className = className;
    if (content) element.textContent = content;

    return element;
}

const save = (equation, result) => {
    const queryHistory = localStorage.getItem("calcHistory");
    const queryHistoryArray = JSON.parse(queryHistory).map(el => new HistoryObject(el.title, el.result));

    const historyEquation = queryHistoryArray.map(el => el.title);

    if (!historyEquation.includes(equation)) {
        queryHistoryArray.push(new HistoryObject(equation, result));
        localStorage.setItem("calcHistory", JSON.stringify(queryHistoryArray.map(el => {return {title: el.title, result: el.result}})));
        updateHistory();
    }
}

const calc = async (calcData) => {
    const data = new FormData();
    data.append("data", calcData);
    data.append("submit", "true");

    const response = await fetch(FORM.action, {
        method: 'POST',
        body: data
    });
    const result = await response.json();

    if (result.status) {
        FORM.querySelector('.calculator-preview').value = result.msg;
        save(calcData, result.msg);
    } else {
        showNotify(result.msg);
    }
};

const removeHistory = (element) => {
    const index = Array.from(element.parentElement.children).indexOf(element);

    const queryHistory = localStorage.getItem("calcHistory");
    let queryHistoryArray = JSON.parse(queryHistory).map(el => new HistoryObject(el.title, el.result));
    queryHistoryArray.splice(-(index + 1), 1);
    localStorage.setItem("calcHistory", JSON.stringify(queryHistoryArray.map(el => {return {title: el.title, result: el.result}})));

};

const updateHistory = () => {
    const historyObject = document.querySelector('.history .menu-list');
    const queryHistory = localStorage.getItem("calcHistory");

    historyObject.innerHTML = "";

    if (queryHistory) {
        const queryHistoryArray = JSON.parse(queryHistory).map(el => new HistoryObject(el.title, el.result));
        if (!queryHistoryArray.length) historyObject.appendChild(createElement("li", null,"История запросов пуста.."));

        for (let i = queryHistoryArray.length - 1; i > queryHistoryArray.length - 9 && i >= 0; i--) {
            historyObject.appendChild(queryHistoryArray[i].getHTML());
        }

        document.querySelectorAll('.history-object').forEach(el => {
            const deleteButton = el.querySelector('.history-delete');
            deleteButton.textContent = "x";
            deleteButton.addEventListener("click", evt => {
                removeHistory(evt.target.parentElement);
                evt.target.parentElement.remove();
            });

            const equationButton = el.querySelector('.history-description');
            equationButton.addEventListener("click", evt => {
                FORM.main.value = evt.currentTarget.dataset.title;
            });
        });
    } else {
        localStorage.setItem("calcHistory", JSON.stringify([]));
        historyObject.appendChild(createElement("li", null, "История запросов пуста.."));
    }
};

const clearHistory = () => {
    const historyObject = document.querySelector('.history .menu-list');
    historyObject.textContent = "";
    historyObject.appendChild(createElement("li", null, "История запросов пуста.."));

    localStorage.setItem("calcHistory", JSON.stringify([]));
};

const showNotify = (text) => {
    const notify = document.querySelector(".notification");
    const notifyContent = notify.querySelector(".notification p");

    notify.classList.add("active");
    notifyContent.textContent = text;
};

const buttonResult = document.querySelector('.button--result');

buttonResult.addEventListener("click", async evt => {
    evt.preventDefault();

    const equationString = FORM.querySelector(`.input[name="main"]`).value;

    await calc(equationString);
});

buttonResult.addEventListener("mouseenter", evt => {
    const button = evt.target;
    if (button.classList.contains("rat")) {
        button.textContent = "Разделить на ноль";
    }
});

buttonResult.addEventListener("mouseleave", evt => {
    const button = evt.target;
    if (button.classList.contains("rat")) {
        button.textContent = "Решить";
    }
});

document.querySelector('.button--clear').addEventListener("click", clearHistory);
document.querySelector('.notification .delete').addEventListener("click", evt => {
    evt.target.parentNode.classList.remove("active");
});

const calculator = new Calculator('main');
calculator.render();
updateHistory();