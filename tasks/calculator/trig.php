<?php

    function getTrig($data) {
        foreach (array('cos', 'sin', 'tan') as $operation) {
            if (strpos($data, $operation) !== false) {
                return $operation;
            }
        }
    }

    function getEndTrigData($data) {
        $count = 0;

        for ($i = 0; $i < strlen($data); $i++) {
            if ($data[$i] == "(") $count++;
            elseif ($data[$i] == ")") {
                if ($count) $count--;
                else return $i;
            }
        }
    }

    function isTrig($data) {
        foreach (array('cos', 'sin', 'tan') as $operation) {
            if (strpos($data, $operation) !== false) return 1;
        }

        return 0;
    }

    function trig($operation, &$data) {
        $trigStartPosition = strpos($data, $operation) + 4;
        $trigLength = getEndTrigData(substr($data, $trigStartPosition));
        $trigEndPosition = $trigStartPosition + $trigLength + 1;

        $trigData = substr($data, $trigStartPosition, $trigLength);

        $trigResult = calculate($trigData);

        $data = substr($data, 0, $trigStartPosition - 4) . $operation($trigResult) . substr($data, $trigEndPosition);
    }

    $trigData = file_get_contents("expression.txt");
    $trigResult = calculate($trigData);
    file_put_contents("result.txt", $trigResult);

?>