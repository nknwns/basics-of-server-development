<?php

include($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$page_title = "Задание #3. Блок-схема";
$is_task_page = true;
$task_name = "Equation";

?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

<main>
    <section class="section block-scheme">
        <div class="container">
            <img src="img/diagram_xl.png" alt="diagram">
        </div>
    </section>
</main>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>