<?php

include($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$page_title = "Задание #3";
$is_task_page = true;
$task_name = "Equation";

?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

    <main>
        <section class="section equation">
            <div class="container">
                <div class="columns">
                    <div class="column is-5 is-offset-2">
                        <form class="form-equation" action="make-equation.php" method="post">
                            <div class="box box-equation">
                                <div class="field">
                                    <div class="buttons has-addons">
                                        <a data-id="1" class="button button-equation is-success is-selected">Ручной</a>
                                        <a data-id="2" class="button button-equation">Полуавтоматический</a>
                                        <a data-id="3" class="button button-equation">Автоматический</a>
                                    </div>
                                </div>
                                <div data-id="1" class="field field-tab active">
                                    <div class="control">
                                        <input class="input input-number" data-type="all" name="equation" type="text" placeholder="Ваше уравнение..">
                                    </div>
                                </div>
                                <div data-id="2" class="field field-tab is-grouped">
                                    <div class="control">
                                        <input class="input input--small input-number" data-type="number" type="text" placeholder="">
                                    </div>
                                    <div class="control">
                                        <input class="input input--small input-number" data-type="operation" type="text" placeholder="">
                                    </div>
                                    <div class="control">
                                        <input class="input input--small input-number" data-type="number" type="text" placeholder="">
                                    </div>
                                    <div class="control">
                                        <input class="input input--small input-number" type="text" value="=" disabled>
                                    </div>
                                    <div class="control">
                                        <input class="input input--small input-number" data-type="number" type="text" placeholder="">
                                    </div>
                                </div>
                                <div data-id="3" class="field field-tab">
                                    <div class="calculator calculator--equation">
                                        <div class="control">
                                            <input type="text" name="equation" class="input calculator-preview" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="buttons buttons-check-result">
                                    <button data-id="1" class="button is-primary button-check-result">Решить</button>
                                    <a class="button is-info" href="block.php">Блок-схема</a>
                                </div>
                            </div>
                            <div class="box equation-result">
                                <label class="label">Решение</label>
                                <p>Уравнение не задано</p>
                            </div>
                        </form>
                    </div>
                    <div class="column is-3">
                        <div class="box history">
                            <div class="menu">
                                <p class="menu-label">История запросов</p>
                                <ul class="menu-list">
                                </ul>
                            </div>
                            <button class="button is-danger is-small button-clear">Очистить</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>