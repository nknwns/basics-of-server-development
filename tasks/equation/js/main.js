class Calculator {
    constructor(className) {
        this.object = document.querySelector(`.calculator--${className}`);
        this.mainInput = this.object.querySelector('.input.calculator-preview')
    }

    handlerOperation(evt) {
        evt.preventDefault();

        this.mainInput.value += evt.target.textContent;
    }

    addHandlersByOperations(objects) {
        objects.forEach(el => el.addEventListener("click", this.handlerOperation.bind(this)));
    }

    getGUI() {
        const GUI = createElement("div", "GUI");

        let buttons = document.createDocumentFragment();
        for (let i = 0; i < 10; i++) buttons.appendChild(createElement("button", "button button-operation", i.toString()));
        const operations = ["+", "-", "*", "/", "=", "X"];
        operations.forEach(el => buttons.appendChild(createElement("button", "button button-operation", el)));

        this.addHandlersByOperations(buttons.childNodes);

        const clearButton = createElement("button", "button button-operation", "C");
        clearButton.addEventListener("click", evt => {
            evt.preventDefault();
            this.mainInput.value = "";
        });
        buttons.appendChild(clearButton);

        GUI.appendChild(buttons);
        return GUI;
    }

    render() {
        this.object.appendChild(this.getGUI());
    }


}

const createElement = (tag, className = null, content = null) => {
    const element = document.createElement(tag);
    if (className) element.className = className;
    if (content) element.textContent = content;

    return element;
}

const createNewHistoryItem = (title) => {
    const elementParent = document.createElement("li");
    const elementItem = document.createElement('a');

    elementItem.textContent = title;
    elementParent.appendChild(elementItem);

    return elementParent;
}

const saveEquation = (equation) => {
    const queryHistory = localStorage.getItem("php-hub_queryHistory");
    const historyObject = document.querySelector('.history .menu-list');
    if (queryHistory) {
        const queryHistoryArray = JSON.parse(queryHistory);
        if (!queryHistoryArray.includes(equation)) {
            queryHistoryArray.push(equation);
            localStorage.setItem("php-hub_queryHistory", JSON.stringify(queryHistoryArray));
            updateHistory();
        }
    }
}

const makeEquation = async (equation) => {
    const data = new FormData();
    data.append("equation", equation);
    data.append("submit", "true");
    const response = await fetch(`http://php-hub.std-1715.ist.mospolytech.ru/tasks/equation/make.php`, {
        method: 'POST',
        body: data
    });
    const result = await response.text();
    document.querySelector('.equation-result p').textContent = result;
    if (result.includes("Деление на ноль")) setTimeout(() => {document.body.classList.add('zero-division')}, 1000);
}

const switchReadType = (id) => {
    document.querySelector('.field-tab.active').classList.remove('active');
    document.querySelector('.button-equation.is-selected').classList.remove('is-selected', 'is-success');

    document.querySelector('.button-check-result').dataset.id = id;
    document.querySelector(`.field-tab[data-id="${id}"]`).classList.add('active');
    document.querySelector(`.button-equation[data-id="${id}"]`).classList.add('is-selected', 'is-success')
}

const updateHistory = () => {
    const historyObject = document.querySelector('.history .menu-list');
    const queryHistory = localStorage.getItem("php-hub_queryHistory");

    historyObject.innerHTML = "";

    if (queryHistory) {
        const queryHistoryArray = JSON.parse(queryHistory);
        if (!queryHistoryArray.length) historyObject.appendChild(createNewHistoryItem("История запросов пуста.."));

        for (let i = queryHistoryArray.length - 1; i > queryHistoryArray.length - 9 && i >= 0; i--) {
            const currentItem = createNewHistoryItem(queryHistoryArray[i]);
            currentItem.addEventListener("click", evt => {
                switchReadType(1);
                document.querySelector('.field-tab[data-id="1"] .input').value = evt.target.textContent;
            });
            historyObject.appendChild(currentItem);
        }
    } else {
        localStorage.setItem("php-hub_queryHistory", JSON.stringify([]));
        historyObject.appendChild(createNewHistoryItem("История запросов пуста.."));
    }
}

const clearHistory = () => {
    const historyObject = document.querySelector('.history .menu-list');
    historyObject.textContent = "";
    historyObject.appendChild(createNewHistoryItem("История запросов пуста.."));

    localStorage.setItem("php-hub_queryHistory", "[]");
}

const isDigit = (symbol) => {
    return correctSymbols.includes(symbol);
}

const checkEquation = (equation) => {
    let isCorrect = true, isOperation = false;

    let parts = ["+", "-", "/", "*", "="].map(el => equation.split(el)).filter(el => el != equation);

    if (!equation.toLowerCase().includes("x")) return 0;
    if (!(parts.length == 2 && equation.includes("="))) return 0;
    parts.forEach(el => el.forEach(symb => {
        if (symb.length) {
            if (!isDigit(symb[0])) isCorrect = false;
        }
    }));

    return isCorrect;
}

const correctSymbols = "1234567890Xx";
const correctOperations = "+-*/";

document.querySelectorAll('.button-equation').forEach(el => {
    el.addEventListener("click", evt => {
        switchReadType(evt.target.dataset.id);
    })

});
document.querySelectorAll('.input-number').forEach(el => {
    el.addEventListener("input", evt => {
        const symbol = evt.data;
        switch (evt.target.dataset.type) {
            case "number": {
                if (!(symbol == null || correctSymbols.includes(symbol))) {
                    evt.target.value = evt.target.value.slice(0, -1);
                }
                break;
            }
            case "operation": {
                if (!(symbol == null || correctOperations.includes(symbol))) {
                    evt.target.value = evt.target.value.slice(0, -1);
                }
                break;
            }
            case "all": {
                if (!(symbol == null || correctOperations.includes(symbol) || correctSymbols.includes(symbol) || symbol == "=")) {
                    evt.target.value = evt.target.value.slice(0, -1);
                }
                break;
            }
        }
    });
});
document.querySelector('.button-check-result').addEventListener("click", async evt => {
    evt.preventDefault();
    const id = evt.target.dataset.id;
    const form = document.querySelector('.form-equation');
    let equationString = "";

    if (id == 2) {
        const inputs = form.querySelectorAll(`.field-tab[data-id="${id}"] .input-number`);
        inputs.forEach(el => equationString += el.value);

    } else {
        equationString = form.querySelector(`.field-tab[data-id="${id}"] .input[name="equation"]`).value;
    }

    if (equationString.length && checkEquation(equationString)) {
        saveEquation(equationString);
        await makeEquation(equationString);
    }
});
document.querySelector('.button-clear').addEventListener("click", clearHistory);

const calculator = new Calculator('equation');
calculator.render();
updateHistory();