<?php
    function zeroDivisionError() {
        echo "Ошибка. Деление на ноль.";
        exit;
    }

    if ($_POST['submit']) {
        $equation = strtolower($_POST["equation"]);

        if (strpos($equation, "/0") !== false) zeroDivisionError();

        $result = 0;
        $operation = "";
        $equation_list = explode("=", $equation);
        $XIndex = strpos($equation_list[0], "x") !== false ? 0 : 1;
        if (strlen($equation_list[$XIndex]) > 1) {
            $result = (int)$equation_list[!$XIndex];
        } else {
            $first = "";
            $second = "";
            $operations = array("+", "-", "*", "/");
            for ($i = 0; $i < strlen($equation_list[!$XIndex]); $i++) {
                if (in_array($equation_list[!$XIndex][$i], $operations)) {
                    $operation = $equation_list[!$XIndex][$i];
                }
            }

            $numbers = explode($operation, $equation_list[!$XIndex]);
            $first = (int)$numbers[0];
            $second = (int)$numbers[1];


            switch ($operation) {
                case "+": {
                    $result = $first + $second;
                    break;
                }
                case "-": {
                    $result = $first - $second;
                    break;
                }
                case "/": {
                    if (!$second) zeroDivisionError();
                    $result = $first / $second;
                    break;
                }
                case "*": {
                    $result = $first * $second;
                    break;
                }
            }
            echo $result;
            exit;
        }



        $operations = array("+", "-", "*", "/");
        for ($i = 0; $i < strlen($equation_list[$XIndex]); $i++) {
            if (in_array($equation_list[$XIndex][$i], $operations)) {
                $operation = $equation_list[$XIndex][$i];
            }
        }
        $numbers = explode($operation, $equation_list[$XIndex]);
        $variableIndex = $numbers[1] == "x";
        switch ($operation) {
            case "+": {
                $result -= (int)$numbers[!$variableIndex];
                break;
            }
            case "-": {
                if ($variableIndex) {
                    $result = (int)$numbers[0] - $result;
                } else {
                    $result += (int)$numbers[1];
                }
                break;
            }
            case "/": {
                if ($variableIndex) {
                    if (!$result) zeroDivisionError();
                    $result = (int)$numbers[0] / $result;
                } else {
                    $result *= (int)$numbers[1];
                }
                break;
            }
            case "*": {
                if ((int)$numbers[!$variableIndex]) zeroDivisionError();
                $result /= (int)$numbers[!$variableIndex];
                break;
            }
        }

        echo $result;
    }

?>