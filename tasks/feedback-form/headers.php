<?php

include($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$page_title = "Задание #2";
$is_task_page = true;
$task_name = "Feedback Form";

$url = "https://httpbin.org";
$headers = get_headers($url, 1);

?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

    <main>
        <section class="section">
            <div class="container">
                <form action="https://httpbin.org/post" method="POST">
                    <div class="field">
                        <label class="label">Список заголовков httpbin.org</label>
                        <div class="control">
                            <textarea style="height: 30em" class="textarea" name="headers" placeholder="Блаблабла"><?php print_r($headers) ?></textarea>
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <div class="control">
                            <a href="<?=$HOSTNAME?>/tasks/feedback-form/index.php" class="button is-link">Вернуться к форме</a>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>