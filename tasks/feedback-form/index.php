<?php

include($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$page_title = "Задание #2";
$is_task_page = true;
$task_name = "Feedback Form";

?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

    <main>
        <section class="section">
            <div class="container">
                <form action="https://httpbin.org/post" method="POST">
                    <div class="field">
                        <label class="label">Имя пользователя</label>
                        <div class="control has-icons-left">
                            <input class="input" name="username" type="text" placeholder="Владимир">
                            <span class="icon is-small is-left">
                              <i class="fas fa-user"></i>
                            </span>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Email</label>
                        <div class="control has-icons-left">
                            <input class="input" name="email" type="email" placeholder="v.v.p@gmail.comm">
                            <span class="icon is-small is-left">
                              <i class="fas fa-envelope"></i>
                            </span>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Тип обращения</label>
                        <div class="control">
                            <div class="select">
                                <select name="type">
                                    <option value="complaint">Жалоба</option>
                                    <option value="suggestion">Предложение</option>
                                    <option value="gratitude">Благодарность</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <label class="label">Текст обращения</label>
                        <div class="control">
                            <textarea class="textarea" name="content" placeholder="Блаблабла"></textarea>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="is-email-response">
                                Ответить мне с помощью e-mail
                            </label>
                        </div>
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="is-sms-response">
                                Ответить мне с помощью СМС
                            </label>
                        </div>
                    </div>


                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-success">Отправить</button>
                        </div>
                        <div class="control">
                            <a href="<?=$HOSTNAME?>/tasks/feedback-form/headers.php" class="button is-link">Просмотреть заголовки</a>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>