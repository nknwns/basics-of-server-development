<?php

include($_SERVER['DOCUMENT_ROOT'] . "/config.php");

$page_title = "Задание #" . 1;
$is_task_page = true;
$task_name = "Hello World";

function get_random_color() {
    $dictionary = array(
        '10' => 'A',
        '11' => 'B',
        '12' => 'C',
        '13' => 'D',
        '14' => 'E',
        '15' => 'F'
    );

    $color = "";

    for ($i = 0; $i < 6; $i++) {
        $number = rand(0, 15);
        $color .= $number < 10 ? $number : $dictionary[$number];
    }

    return $color;
}

$random_color = get_random_color();
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

    <main>
        <section class="section section-center">
            <h2 class="color-title">Сгенерированный цвет: #<?=$random_color?></h2>
            <div class="color-block" style="background-color: #<?=$random_color?>"></div>
        </section>
    </main>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>